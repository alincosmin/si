﻿using System;
using System.Text;

namespace MyAES.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var key = "abcde";
            var inputText =
                "If you want to encrypt a text put it in the white textarea above, set the key of the encryption then push the Encrypt button.";

            var cipher = MyAES.Encrypt(inputText, key);
            var plain = MyAES.Decrypt(cipher, key);

            Console.WriteLine("Message:\n\t {0}\nKey: \n\t{1}\nCipher:\n\t{2}\nDecoded:\n\t{3}", inputText,
                key, cipher, plain);


        }

    }

}
